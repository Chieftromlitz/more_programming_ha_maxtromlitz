﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mazegen : MonoBehaviour {

	[SerializeField] GameObject m_mazeElement_A;
	[SerializeField] GameObject m_mazeElement_B;
	[SerializeField] GameObject m_mazeElement_C;
	[SerializeField] GameObject m_mazeElement_D;
	[SerializeField] GameObject m_mazeElement_E;
	[SerializeField] GameObject m_mazeElement_F;

	[SerializeField] int m_mazeWidth;
	[SerializeField] int m_mazeHeight;

	[SerializeField] float m_distanceMazeElements;

	// Use this for initialization
	void Start () {

		GenerateMaze ();
	}

	void GenerateMaze()
	{
		Vector3 elementPosition = Vector3.zero  ;
		GameObject elementObject = null;
		int counter_width = 0; 
		int counter_height = 0; 

		while(counter_width < m_mazeWidth) 
		{
			while (counter_height < m_mazeHeight) 
			{
				elementPosition.x = (counter_width - (m_mazeWidth / 2)) * m_distanceMazeElements;
				elementPosition.z = (counter_height - (m_mazeHeight / 2)) * m_distanceMazeElements;

				float randomValue = Random.value;
				if (randomValue <= (1f/6f) * 1f) {
					elementObject = Instantiate (m_mazeElement_A, elementPosition, m_mazeElement_A.transform.rotation);
					elementObject.name = "MazeElement_A_";
				} else if (randomValue <= (1f/6f) * 2f) {
					elementObject = Instantiate (m_mazeElement_B, elementPosition, m_mazeElement_B.transform.rotation);
					elementObject.name = "MazeElement_B_";
				} else if (randomValue <= (1f/6f) * 3f) {
					elementObject = Instantiate (m_mazeElement_C, elementPosition, m_mazeElement_C.transform.rotation);
					elementObject.name = "MazeElement_C_";
				} else if (randomValue <= (1f/6f) * 4f) {
					elementObject = Instantiate (m_mazeElement_D, elementPosition, m_mazeElement_D.transform.rotation);
					elementObject.name = "MazeElement_D_";
				} else if (randomValue <= (1f/6f) * 5f) {
					elementObject = Instantiate (m_mazeElement_E, elementPosition, m_mazeElement_E.transform.rotation);
					elementObject.name = "MazeElement_E_";
				} else  {
					elementObject = Instantiate (m_mazeElement_F, elementPosition, m_mazeElement_F.transform.rotation);
					elementObject.name = "MazeElement_F_";
				} 


				elementObject.name += counter_width + "_" + counter_height;

				counter_height++;
			}

			counter_height = 0;
			counter_width++;

		}
	}
}
